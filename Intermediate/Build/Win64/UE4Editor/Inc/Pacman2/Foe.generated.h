// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PACMAN2_Foe_generated_h
#error "Foe.generated.h already included, missing '#pragma once' in Foe.h"
#endif
#define PACMAN2_Foe_generated_h

#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_SPARSE_DATA
#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnCollision) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCollision(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnCollision) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCollision(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFoe(); \
	friend struct Z_Construct_UClass_AFoe_Statics; \
public: \
	DECLARE_CLASS(AFoe, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman2"), NO_API) \
	DECLARE_SERIALIZER(AFoe)


#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAFoe(); \
	friend struct Z_Construct_UClass_AFoe_Statics; \
public: \
	DECLARE_CLASS(AFoe, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman2"), NO_API) \
	DECLARE_SERIALIZER(AFoe)


#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFoe(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFoe) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoe); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoe); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoe(AFoe&&); \
	NO_API AFoe(const AFoe&); \
public:


#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoe(AFoe&&); \
	NO_API AFoe(const AFoe&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoe); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoe); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFoe)


#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_PRIVATE_PROPERTY_OFFSET
#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_11_PROLOG
#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_PRIVATE_PROPERTY_OFFSET \
	Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_SPARSE_DATA \
	Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_RPC_WRAPPERS \
	Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_INCLASS \
	Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_PRIVATE_PROPERTY_OFFSET \
	Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_SPARSE_DATA \
	Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_INCLASS_NO_PURE_DECLS \
	Pacman2_4_24___2_Source_Pacman2_Public_Foe_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PACMAN2_API UClass* StaticClass<class AFoe>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pacman2_4_24___2_Source_Pacman2_Public_Foe_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
