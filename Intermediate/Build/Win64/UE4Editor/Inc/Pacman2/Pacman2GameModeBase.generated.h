// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PACMAN2_Pacman2GameModeBase_generated_h
#error "Pacman2GameModeBase.generated.h already included, missing '#pragma once' in Pacman2GameModeBase.h"
#endif
#define PACMAN2_Pacman2GameModeBase_generated_h

#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_SPARSE_DATA
#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_RPC_WRAPPERS
#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPacman2GameModeBase(); \
	friend struct Z_Construct_UClass_APacman2GameModeBase_Statics; \
public: \
	DECLARE_CLASS(APacman2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman2"), NO_API) \
	DECLARE_SERIALIZER(APacman2GameModeBase)


#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAPacman2GameModeBase(); \
	friend struct Z_Construct_UClass_APacman2GameModeBase_Statics; \
public: \
	DECLARE_CLASS(APacman2GameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman2"), NO_API) \
	DECLARE_SERIALIZER(APacman2GameModeBase)


#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APacman2GameModeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APacman2GameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APacman2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APacman2GameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APacman2GameModeBase(APacman2GameModeBase&&); \
	NO_API APacman2GameModeBase(const APacman2GameModeBase&); \
public:


#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APacman2GameModeBase(APacman2GameModeBase&&); \
	NO_API APacman2GameModeBase(const APacman2GameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APacman2GameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APacman2GameModeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APacman2GameModeBase)


#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_PRIVATE_PROPERTY_OFFSET
#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_18_PROLOG
#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_PRIVATE_PROPERTY_OFFSET \
	Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_SPARSE_DATA \
	Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_RPC_WRAPPERS \
	Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_INCLASS \
	Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_PRIVATE_PROPERTY_OFFSET \
	Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_SPARSE_DATA \
	Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_INCLASS_NO_PURE_DECLS \
	Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PACMAN2_API UClass* StaticClass<class APacman2GameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pacman2_4_24___2_Source_Pacman2_Pacman2GameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
