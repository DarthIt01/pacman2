// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PACMAN2_PHud_generated_h
#error "PHud.generated.h already included, missing '#pragma once' in PHud.h"
#endif
#define PACMAN2_PHud_generated_h

#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_SPARSE_DATA
#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_RPC_WRAPPERS
#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPHud(); \
	friend struct Z_Construct_UClass_APHud_Statics; \
public: \
	DECLARE_CLASS(APHud, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman2"), NO_API) \
	DECLARE_SERIALIZER(APHud)


#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAPHud(); \
	friend struct Z_Construct_UClass_APHud_Statics; \
public: \
	DECLARE_CLASS(APHud, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman2"), NO_API) \
	DECLARE_SERIALIZER(APHud)


#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APHud(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APHud) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APHud); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APHud); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APHud(APHud&&); \
	NO_API APHud(const APHud&); \
public:


#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APHud(APHud&&); \
	NO_API APHud(const APHud&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APHud); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APHud); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APHud)


#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__HUDFont() { return STRUCT_OFFSET(APHud, HUDFont); }


#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_9_PROLOG
#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_PRIVATE_PROPERTY_OFFSET \
	Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_SPARSE_DATA \
	Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_RPC_WRAPPERS \
	Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_INCLASS \
	Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_PRIVATE_PROPERTY_OFFSET \
	Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_SPARSE_DATA \
	Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_INCLASS_NO_PURE_DECLS \
	Pacman2_4_24___2_Source_Pacman2_Public_PHud_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PACMAN2_API UClass* StaticClass<class APHud>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pacman2_4_24___2_Source_Pacman2_Public_PHud_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
