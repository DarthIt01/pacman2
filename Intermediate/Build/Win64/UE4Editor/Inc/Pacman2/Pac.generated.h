// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PACMAN2_Pac_generated_h
#error "Pac.generated.h already included, missing '#pragma once' in Pac.h"
#endif
#define PACMAN2_Pac_generated_h

#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_SPARSE_DATA
#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnCollision) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCollision(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnCollision) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OverlappedComponent); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComponent); \
		P_GET_PROPERTY(UIntProperty,Z_Param_OtherBodyIndex); \
		P_GET_UBOOL(Z_Param_bFromSweep); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_SweepResult); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnCollision(Z_Param_OverlappedComponent,Z_Param_OtherActor,Z_Param_OtherComponent,Z_Param_OtherBodyIndex,Z_Param_bFromSweep,Z_Param_Out_SweepResult); \
		P_NATIVE_END; \
	}


#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPac(); \
	friend struct Z_Construct_UClass_APac_Statics; \
public: \
	DECLARE_CLASS(APac, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman2"), NO_API) \
	DECLARE_SERIALIZER(APac)


#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAPac(); \
	friend struct Z_Construct_UClass_APac_Statics; \
public: \
	DECLARE_CLASS(APac, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Pacman2"), NO_API) \
	DECLARE_SERIALIZER(APac)


#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APac(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APac) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APac); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APac); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APac(APac&&); \
	NO_API APac(const APac&); \
public:


#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APac(APac&&); \
	NO_API APac(const APac&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APac); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APac); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APac)


#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_PRIVATE_PROPERTY_OFFSET
#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_10_PROLOG
#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_PRIVATE_PROPERTY_OFFSET \
	Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_SPARSE_DATA \
	Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_RPC_WRAPPERS \
	Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_INCLASS \
	Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_PRIVATE_PROPERTY_OFFSET \
	Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_SPARSE_DATA \
	Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_INCLASS_NO_PURE_DECLS \
	Pacman2_4_24___2_Source_Pacman2_Public_Pac_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PACMAN2_API UClass* StaticClass<class APac>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Pacman2_4_24___2_Source_Pacman2_Public_Pac_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
