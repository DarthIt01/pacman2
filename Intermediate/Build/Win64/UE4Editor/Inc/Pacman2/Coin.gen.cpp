// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Pacman2/Public/Coin.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCoin() {}
// Cross Module References
	PACMAN2_API UClass* Z_Construct_UClass_ACoin_NoRegister();
	PACMAN2_API UClass* Z_Construct_UClass_ACoin();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Pacman2();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USphereComponent_NoRegister();
// End Cross Module References
	void ACoin::StaticRegisterNativesACoin()
	{
	}
	UClass* Z_Construct_UClass_ACoin_NoRegister()
	{
		return ACoin::StaticClass();
	}
	struct Z_Construct_UClass_ACoin_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSuperCollectable5_MetaData[];
#endif
		static void NewProp_bIsSuperCollectable5_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSuperCollectable5;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSuperCollectable4_MetaData[];
#endif
		static void NewProp_bIsSuperCollectable4_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSuperCollectable4;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSuperCollectable3_MetaData[];
#endif
		static void NewProp_bIsSuperCollectable3_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSuperCollectable3;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSuperCollectable2_MetaData[];
#endif
		static void NewProp_bIsSuperCollectable2_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSuperCollectable2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSuperCollectable_MetaData[];
#endif
		static void NewProp_bIsSuperCollectable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSuperCollectable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollectableMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollectableMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseCollisionComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BaseCollisionComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACoin_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Pacman2,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Coin.h" },
		{ "ModuleRelativePath", "Public/Coin.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable5_MetaData[] = {
		{ "Category", "Coin" },
		{ "Comment", "// If Pacman gets a coin with 5th field active, Droids get slower \n" },
		{ "ModuleRelativePath", "Public/Coin.h" },
		{ "ToolTip", "If Pacman gets a coin with 5th field active, Droids get slower" },
	};
#endif
	void Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable5_SetBit(void* Obj)
	{
		((ACoin*)Obj)->bIsSuperCollectable5 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable5 = { "bIsSuperCollectable5", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ACoin), &Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable5_SetBit, METADATA_PARAMS(Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable5_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable5_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable4_MetaData[] = {
		{ "Category", "Coin" },
		{ "Comment", "// With the 4th field of the coin, Droids are destroyable \n" },
		{ "ModuleRelativePath", "Public/Coin.h" },
		{ "ToolTip", "With the 4th field of the coin, Droids are destroyable" },
	};
#endif
	void Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable4_SetBit(void* Obj)
	{
		((ACoin*)Obj)->bIsSuperCollectable4 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable4 = { "bIsSuperCollectable4", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ACoin), &Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable4_SetBit, METADATA_PARAMS(Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable4_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable4_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable3_MetaData[] = {
		{ "Category", "Coin" },
		{ "Comment", "// 3rd field is for making Pacman faster\n" },
		{ "ModuleRelativePath", "Public/Coin.h" },
		{ "ToolTip", "3rd field is for making Pacman faster" },
	};
#endif
	void Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable3_SetBit(void* Obj)
	{
		((ACoin*)Obj)->bIsSuperCollectable3 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable3 = { "bIsSuperCollectable3", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ACoin), &Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable3_SetBit, METADATA_PARAMS(Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable3_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable3_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable2_MetaData[] = {
		{ "Category", "Coin" },
		{ "Comment", "// First one increase Pacman's health, 2nd is for invulnerability\n" },
		{ "ModuleRelativePath", "Public/Coin.h" },
		{ "ToolTip", "First one increase Pacman's health, 2nd is for invulnerability" },
	};
#endif
	void Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable2_SetBit(void* Obj)
	{
		((ACoin*)Obj)->bIsSuperCollectable2 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable2 = { "bIsSuperCollectable2", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ACoin), &Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable2_SetBit, METADATA_PARAMS(Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable_MetaData[] = {
		{ "Category", "Coin" },
		{ "Comment", "// Creating fields for each special Coin\n" },
		{ "ModuleRelativePath", "Public/Coin.h" },
		{ "ToolTip", "Creating fields for each special Coin" },
	};
#endif
	void Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable_SetBit(void* Obj)
	{
		((ACoin*)Obj)->bIsSuperCollectable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable = { "bIsSuperCollectable", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ACoin), &Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable_SetBit, METADATA_PARAMS(Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_Statics::NewProp_CollectableMesh_MetaData[] = {
		{ "Category", "Coin" },
		{ "Comment", "// Creating mesh component of the Coin\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Coin.h" },
		{ "ToolTip", "Creating mesh component of the Coin" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACoin_Statics::NewProp_CollectableMesh = { "CollectableMesh", nullptr, (EPropertyFlags)0x0010000000090009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ACoin, CollectableMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACoin_Statics::NewProp_CollectableMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACoin_Statics::NewProp_CollectableMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACoin_Statics::NewProp_BaseCollisionComponent_MetaData[] = {
		{ "Category", "Coin" },
		{ "Comment", "// Creating collision component of the Coin\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Coin.h" },
		{ "ToolTip", "Creating collision component of the Coin" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACoin_Statics::NewProp_BaseCollisionComponent = { "BaseCollisionComponent", nullptr, (EPropertyFlags)0x0010000000090009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ACoin, BaseCollisionComponent), Z_Construct_UClass_USphereComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACoin_Statics::NewProp_BaseCollisionComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACoin_Statics::NewProp_BaseCollisionComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACoin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable5,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable4,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable3,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_Statics::NewProp_bIsSuperCollectable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_Statics::NewProp_CollectableMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACoin_Statics::NewProp_BaseCollisionComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACoin_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACoin>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACoin_Statics::ClassParams = {
		&ACoin::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ACoin_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ACoin_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ACoin_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ACoin_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACoin()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACoin_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACoin, 2990901843);
	template<> PACMAN2_API UClass* StaticClass<ACoin>()
	{
		return ACoin::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACoin(Z_Construct_UClass_ACoin, &ACoin::StaticClass, TEXT("/Script/Pacman2"), TEXT("ACoin"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACoin);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
